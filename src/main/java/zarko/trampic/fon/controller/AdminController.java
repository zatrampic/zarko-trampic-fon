package zarko.trampic.fon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;

@Controller
@RequestMapping(value = "admin")
public class AdminController extends ControllerModelAttributes {

    @GetMapping
    public String getAdmin(Model model) {
        return "admin";
    }
}
