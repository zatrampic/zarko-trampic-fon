package zarko.trampic.fon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;
import zarko.trampic.fon.dto.*;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.facade.ExamFacade;
import zarko.trampic.fon.facade.ProfessorFacade;
import zarko.trampic.fon.facade.SubjectFacade;

import javax.validation.Valid;
import java.util.Set;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping(value = "exam")
public class ExamController extends ControllerModelAttributes {
    private final ExamFacade examFacade;
    private final ProfessorFacade professorFacade;
    private final SubjectFacade subjectFacade;
    @Autowired
    public ExamController(ExamFacade examFacade, ProfessorFacade professorFacade, SubjectFacade subjectFacade) {
        this.examFacade = examFacade;
        this.professorFacade = professorFacade;
        this.subjectFacade = subjectFacade;
    }
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }
    @ModelAttribute("professorForExam")
    public Set<ProfessorDto> professorDtos() {
        return professorFacade.getAll();
    }
    @ModelAttribute("subjectsForExam")
    public Set<SubjectDto> subjectsForExam() {
        return subjectFacade.getAll();
    }

    @PostMapping("/insert")
    public String insertExam(@ModelAttribute @Valid ExamDto examDto, BindingResult result, Model model) {
            if(result.hasErrors())return "admin";
        try {
            examFacade.insertExam(examDto);
            model.addAttribute("message", Messages.EXAM_INSERT_SUCCESS.getMessage());
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message", Messages.EXAM_INSERT_FAILED.getMessage());
            return "admin";
        }
    }
    @GetMapping("/initExam")
    public String prepareExamPage(Model model) {
        System.out.println("ODJE SAM");
        return "admin";
    }
    @GetMapping("/getAll/{pageSize}/{pageNumber}")
    public String getAll(@PathVariable(name = "pageSize") Integer pageSize,@PathVariable(name = "pageNumber") Integer pageNumber, Model model) {
        try {
            Set<ExamDtoResponse> searchedExams = examFacade.getAll(pageNumber, pageSize);
            model.addAttribute("searchedExams", searchedExams);
            return "admin";
        } catch (Exception e) {
            return "admin";
        }
    }
    /*
--------------- H A R D   C O D E  PAGINATION---------
    ( #TODO front jsp pagination )
*/
    @GetMapping("/profId/{id}")
    public String getExamByProfessor(@PathVariable(name = "id") Long id, Model model) {
        Integer pageNumber = 0;
        Integer pageSize = 10000;
        try {
            Set<ExamDtoResponse> searchByProf = examFacade.findByProfessorId(id, pageNumber, pageSize);
            model.addAttribute("searchedExams", searchByProf);
            return "admin";
        } catch (Exception e) {
            return "admin";
        }
    }
    @GetMapping("/examId/{id}")
    public String getExamSbySubject(@PathVariable(name = "id") Long id, Model model) {
        Integer pageNumber = 0;
        Integer pageSize = 10000;
        try {
            Set<ExamDtoResponse> searchedExams = examFacade.findBysubjectId(id,pageNumber, pageSize);
            model.addAttribute("searchedExams", searchedExams);
            return "admin";
        } catch (Exception e) {
            return "admin";
        }
    }

}
