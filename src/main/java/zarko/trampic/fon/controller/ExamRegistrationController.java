package zarko.trampic.fon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;
import zarko.trampic.fon.dto.*;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.facade.ExamRegistrationFacade;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/examRegistration")
public class ExamRegistrationController extends ControllerModelAttributes {
    private final ExamRegistrationFacade examRegistrationFacade;
    @Autowired
    public ExamRegistrationController(ExamRegistrationFacade examRegistrationFacade) {
        this.examRegistrationFacade = examRegistrationFacade;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @PostMapping("/insert")
    public String insertExamRegistration(@ModelAttribute @Valid ExamRegistrationDtoRequest examRegistrationDtoRequest, BindingResult result, Model model){
        if(result.hasErrors()) return "admin";
            try {
                examRegistrationFacade.insertExamRegistration(examRegistrationDtoRequest);
                model.addAttribute("message", Messages.INSERT_REGISTRATION_EXAM_SUCCES.getMessage());
                return "admin";
            }catch (Exception e){
                model.addAttribute("message",e.getMessage());
                return "admin";
            }

    }
}
