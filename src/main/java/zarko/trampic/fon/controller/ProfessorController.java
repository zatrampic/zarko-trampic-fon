package zarko.trampic.fon.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;
import zarko.trampic.fon.dto.*;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.facade.ProfessorFacade;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Set;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping(value = "professor")
public class ProfessorController extends ControllerModelAttributes {

    private final ProfessorFacade professorFacade;
    @Autowired
    public ProfessorController(ProfessorFacade professorFacade) {
        this.professorFacade = professorFacade;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }
    @PostMapping("/insert")
    public String insertProfessor(@ModelAttribute @Valid ProfessorDto professorDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "admin";
        }
        try {
            professorFacade.insertNew(professorDto);
            model.addAttribute("message", Messages.PROFESSOR_ADD_SUCCESS.getMessage());
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message", Messages.PROFESSOR_ADD_FAILED.getMessage());
            return "admin";
        }
    }

    @GetMapping("/getAll")
    public String getAll(Model model) {
        Set<ProfessorDto> professorDtos = professorFacade.getAll();
        model.addAttribute("setProfessors", professorDtos);
        return "admin";
    }

    @GetMapping("/delete/{id}")
    public String deleteProfessor(@PathVariable(name = "id") Long id, Model model, HttpServletRequest request) {
        try {
            professorFacade.deleteById(id);
            model.addAttribute("message", Messages.DELETE_PROFESSOR_SUCCESS.getMessage());
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message", Messages.SOMETHING_WENT_WRONG.getMessage());
            return "admin";
        }
    }

    @PostMapping("/update")
    public String updateProfessor(@ModelAttribute @Valid ProfessorDto professorDto,BindingResult result,Model model) {
        if (result.hasErrors()) {
            return "admin";
        }
        try{
            professorFacade.update(professorDto);
            model.addAttribute("message", Messages.PROFESSOR_UPDATE_SUCCESS.getMessage());
            model.addAttribute("professorDto",new ProfessorDto());
            return "admin";
        }catch (Exception e){
            model.addAttribute("message", Messages.PROFESSOR_UPDATE_FAILED.getMessage());
            return "admin";
        }
    }
}
