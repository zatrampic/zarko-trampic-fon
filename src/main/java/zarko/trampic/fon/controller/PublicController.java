package zarko.trampic.fon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;
import zarko.trampic.fon.dto.*;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.facade.UserFacade;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class PublicController extends ControllerModelAttributes{
    private final UserFacade userFacade;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public PublicController(UserFacade userFacade, AuthenticationManager authenticationManager) {
        this.userFacade = userFacade;
        this.authenticationManager = authenticationManager;
    }
    @GetMapping("/")
    public String home() {
        System.out.println("OVDE SAM");
        return "home";
    }
    @GetMapping("/login")
    public String getLogin(Model model) {
        model.addAttribute("credentials", new UserCredentials());
        return "login";
    }
    @PostMapping("/login")
    public String login(@ModelAttribute UserCredentials credentials, BindingResult result, Model model, HttpServletRequest request) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            credentials.getUsername(),
                            credentials.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message",Messages.BAD_CREDENTIALS.getMessage());
            model.addAttribute("credentials", new UserCredentials());
            return "login";
        }
    }

    @GetMapping("/singUp")
    public String getSingUp(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "singUp";
    }

    @PostMapping("/singUp")
    public ModelAndView singUp(@ModelAttribute @Valid UserDto userDto, BindingResult result) {
        if (result.hasErrors()) return new ModelAndView("singUp");
        try {
            userFacade.saveNewUser(userDto);
            return new ModelAndView("home").addObject("message", Messages.REGISTRATION_SUCCESS.getMessage());
        } catch (Exception e) {
            return new ModelAndView("home").addObject("message", Messages.USER_ALREADY_EXIST.getMessage());
        }
    }
}
