package zarko.trampic.fon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;
import zarko.trampic.fon.dto.*;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.facade.StudentFacade;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Set;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping(value = "student")
public class StudentController extends ControllerModelAttributes{
    private final StudentFacade studentFacade;

    @Autowired
    public StudentController(StudentFacade studentFacade) {
        this.studentFacade = studentFacade;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @PostMapping("/insert")
    public String insertStudent(@ModelAttribute @Valid StudentDto studentDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "admin";
        }
        try {
            studentFacade.insertNewStudent(studentDto);
            model.addAttribute("message", Messages.STUDENT_INSERT_SUCCESS.getMessage());
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message", Messages.STUDENT_INSERT_FAILED.getMessage());
            return "admin";
        }
    }
    @GetMapping("/getAll")
    public String getAll(Model model){
        Set<StudentDto> studentDtoSet = studentFacade.getAll();
        model.addAttribute("studentDtoSet",studentDtoSet);
        return "admin";
    }

    @GetMapping("/delete/{id}")
    public String deleteStudent(@PathVariable(name = "id") Long id, Model model, HttpServletRequest request) {
        try {
            studentFacade.deleteById(id);
            model.addAttribute("message", Messages.DELETE_STUDENT_SUCCESS.getMessage());
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message", Messages.SOMETHING_WENT_WRONG.getMessage());
            return "admin";
        }
    }
}
