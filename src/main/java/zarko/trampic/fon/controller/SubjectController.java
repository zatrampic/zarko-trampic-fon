package zarko.trampic.fon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import zarko.trampic.fon.controller.models.ControllerModelAttributes;
import zarko.trampic.fon.dto.*;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.facade.SubjectFacade;
import javax.validation.Valid;
import java.util.Set;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping(value = "subject")
public class SubjectController extends ControllerModelAttributes {

    private final SubjectFacade subjectFacade;
    @Autowired
    public SubjectController(SubjectFacade subjectFacade) {
        this.subjectFacade = subjectFacade;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @PostMapping("/insert")
    public String insertSubject(@ModelAttribute @Valid SubjectDto subjectDto, BindingResult result, Model model){
        if (result.hasErrors()) return "admin";
        try {
            subjectFacade.insertNew(subjectDto);
            model.addAttribute("message",Messages.SUBJECT_INSERT_SUCCESS.getMessage());
            return "admin";
        }catch (Exception e){
            model.addAttribute("message", Messages.SUBJECT_INSERT_FAILED.getMessage());
            return "admin";
        }
    }

    @GetMapping("/getAll")
    public String getAll(Model model){
        try{
            Set<SubjectDto> subjectDtoSet = subjectFacade.getAll();
            model.addAttribute("subjectDtoSet",subjectDtoSet);
            return "admin";
        }catch (Exception e){
            return "admin";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteSubject(@PathVariable(name = "id") Long id, Model model) {
        try {
            subjectFacade.deleteById(id);
            model.addAttribute("message", Messages.DELETE_SUBJECT_SUCCESS.getMessage());
            return "admin";
        } catch (Exception e) {
            model.addAttribute("message", Messages.SOMETHING_WENT_WRONG.getMessage());
            return "admin";
        }
    }
}
