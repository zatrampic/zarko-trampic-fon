package zarko.trampic.fon.controller.models;

import org.springframework.web.bind.annotation.ModelAttribute;
import zarko.trampic.fon.dto.*;

public class ControllerModelAttributes {
    @ModelAttribute("subjectDto")
    public SubjectDto getSubjectDto(){
        return new SubjectDto();
    }
    @ModelAttribute("professorDto")
    public ProfessorDto professorDto() {
        return new ProfessorDto();
    }
    @ModelAttribute("studentDto")
    public StudentDto getStudentDto() {
        return new StudentDto();
    }
    @ModelAttribute("examDto")
    public ExamDto examDto() {
        return new ExamDto();
    }
    @ModelAttribute("examRegistrationRequestDto")
    public ExamRegistrationDtoRequest examRegistrationDtoRequest(){
        return new ExamRegistrationDtoRequest();
    }
}
