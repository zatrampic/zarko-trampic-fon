package zarko.trampic.fon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zarko.trampic.fon.dto.ExamDtoResponse;
import zarko.trampic.fon.entity.Exam;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ExamConverter {
    private final ProfessorConverter professorConverter;
    private final SubjectConverter subjectConverter;
    private ExamRegistrationConverter examRegistrationConverter;
    @Autowired
    public ExamConverter(ProfessorConverter professorConverter, SubjectConverter subjectConverter) {
        this.professorConverter = professorConverter;
        this.subjectConverter = subjectConverter;
    }
    @Autowired
    public void setExamRegistrationConverter(ExamRegistrationConverter examRegistrationConverter) {
        this.examRegistrationConverter = examRegistrationConverter;
    }

    public Exam convertToEntity(Long examId) {
        Exam exam = new Exam();
        exam.setIdExam(examId);
        return exam;
    }

    public ExamDtoResponse convertToExamDtoResponse(Exam exam){
        ExamDtoResponse examDtoResponse = new ExamDtoResponse();
        examDtoResponse.setIdExamDtoResponse(exam.getIdExam());
        examDtoResponse.setDateOfExam(convertToStringDate(exam.getExamDate()));
        examDtoResponse.setProfessorDto(professorConverter.convertToDto(exam.getProfessor()));
        examDtoResponse.setExpired(convertIsExpired(exam.isExpired()));
        examDtoResponse.setSubjectDto(subjectConverter.convertToDto(exam.getSubject()));
        examDtoResponse.setSetRegistrations(examRegistrationConverter.convertToDtoSet(exam.getExamRegistrations()));
        return examDtoResponse;
    }

    public Set<ExamDtoResponse> convertToExamDtoResponseSet(Set<Exam> exams) {
        Set<ExamDtoResponse> dtoResponses = exams.stream().map(exam -> {
            ExamDtoResponse examDtoResponse = convertToExamDtoResponse(exam);
            return examDtoResponse;
        }).collect(Collectors.toSet());
        return dtoResponses;
    }

    private String convertToStringDate(Date examDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(examDate);
    }

    private String convertIsExpired(boolean expired) {
        if(expired){
            return "Expired";
        }else {
            return "Non expired";
        }
    }

}
