package zarko.trampic.fon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zarko.trampic.fon.dto.ExamRegistrationDtoRequest;
import zarko.trampic.fon.entity.ExamRegistration;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ExamRegistrationConverter {
    private final ExamConverter examConverter;
    @Autowired
    public ExamRegistrationConverter(ExamConverter examConverter) {
        this.examConverter = examConverter;
    }

    public ExamRegistration convertToEntity(ExamRegistrationDtoRequest examRegistrationDtoRequest) throws ParseException {
        ExamRegistration examRegistration = new ExamRegistration();
        examRegistration.setDateOfRegistration(convertToDate(examRegistrationDtoRequest.getDateOfRegistration()));
        examRegistration.setStudentIndex(examRegistrationDtoRequest.getStudentIndex());
        examRegistration.setExam(examConverter.convertToEntity(examRegistrationDtoRequest.getExamId()));
        return examRegistration;
    }

    private Date convertToDate(String dateOfRegistration) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(dateOfRegistration);
        return date;
    }

    public ExamRegistrationDtoRequest convertToDto(ExamRegistration examRegistration){
        ExamRegistrationDtoRequest dtoRequest = new ExamRegistrationDtoRequest();
        dtoRequest.setDateOfRegistration(examRegistration.getDateOfRegistration().toString());
        dtoRequest.setStudentIndex(examRegistration.getStudentIndex());
        return dtoRequest;
    }

    public Set<ExamRegistrationDtoRequest> convertToDtoSet(Set<ExamRegistration> examRegistrations) {
        Set<ExamRegistrationDtoRequest> examRegistrationDtoRequests = examRegistrations.stream().map(examRegistration -> {
            ExamRegistrationDtoRequest dtoRequest = convertToDto(examRegistration);
            return dtoRequest;
        }).collect(Collectors.toSet());
        return examRegistrationDtoRequests;
    }
}
