package zarko.trampic.fon.converter;

import org.springframework.stereotype.Service;
import zarko.trampic.fon.entity.Place;

@Service
public class PlaceConverter {

    public Place convertToEntity(String city) {
        Place place = new Place();
        place.setPostNumber("10000");
        place.setCity(city);
        return place;
    }
}
