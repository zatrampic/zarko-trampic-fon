package zarko.trampic.fon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zarko.trampic.fon.dto.ProfessorDto;
import zarko.trampic.fon.entity.Professor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProfessorConverter {
    private final PlaceConverter placeConverter;
    private final TitleConverter titleConverter;
    @Autowired
    public ProfessorConverter(PlaceConverter placeConverter, TitleConverter titleConverter) {
        this.placeConverter = placeConverter;
        this.titleConverter = titleConverter;
    }

    public Professor convertToEntity(ProfessorDto professorDto) throws ParseException {
        Professor professor = new Professor();
        if(professorDto.getId() != null) professor.setIdProfessor(professorDto.getId());
        if(professorDto.getCity() != null) professor.setCity(placeConverter.convertToEntity(professorDto.getCity()));
        if(professorDto.getTitle() != null) professor.setTitle(titleConverter.convertToEntity(professorDto.getTitle()));
        professor.setFirstName(professorDto.getFirstName());
        professor.setLastName(professorDto.getLastName());
        professor.setAddress(professorDto.getAddress());
        professor.setEmail(professorDto.getEmail());
        professor.setPhoneNumber(professorDto.getPhone());
        professor.setReelectionDate(formatStringToDate(professorDto.getReelectionDate()));
        return professor;
    }

    private Date formatStringToDate(String reelectionDate) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(reelectionDate);
        return date;
    }

    public ProfessorDto convertToDto(Professor professor){
        ProfessorDto professorDto = new ProfessorDto();
        professorDto.setId(professor.getIdProfessor());
        professorDto.setAddress(professor.getAddress());
        professorDto.setTitle(professor.getTitle().getTitleName().toString());
        professorDto.setCity(professor.getCity().getCity());
        professorDto.setEmail(professor.getEmail());
        professorDto.setFirstName(professor.getFirstName());
        professorDto.setLastName(professor.getLastName());
        professorDto.setReelectionDate(professor.getReelectionDate().toString());
        professorDto.setPhone(professor.getPhoneNumber());
        return professorDto;

    }

    public Set<ProfessorDto> convertToDTOsSet(Set<Professor> professorSet) {
        Set<ProfessorDto> professorDtos = professorSet.stream().map(professor -> {
            ProfessorDto professorDto = convertToDto(professor);
            return professorDto;
        }).collect(Collectors.toSet());
        return professorDtos;
    }
}
