package zarko.trampic.fon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zarko.trampic.fon.dto.StudentDto;
import zarko.trampic.fon.entity.Student;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StudentConverter {
    private final PlaceConverter placeConverter;
    @Autowired
    public StudentConverter(PlaceConverter placeConverter) {
        this.placeConverter = placeConverter;
    }

    public Student convertToEntity(StudentDto studentDto) {
        Student student = new Student();
        if(studentDto.getIdStudent() != null) student.setIdStudent(studentDto.getIdStudent());
        student.setAddress(studentDto.getAddress());
        student.setCurrentYearOfStudy(Long.parseLong(String.valueOf(studentDto.getCurrentYearOfStudy())));
        student.setEmail(studentDto.getEmail());
        student.setFirstName(studentDto.getFirstName());
        student.setLastName(studentDto.getLastName());
        student.setIndexNumber(studentDto.getIndexNumber());
        student.setPhoneNumber(studentDto.getPhone());
        if(studentDto.getCity() != null) student.setCity(placeConverter.convertToEntity(studentDto.getCity()));
        return student;
    }

    public StudentDto convertToDto(Student student){
        StudentDto studentDto = new StudentDto();
        studentDto.setIdStudent(student.getIdStudent());
        studentDto.setAddress(student.getAddress());
        studentDto.setCurrentYearOfStudy(Integer.parseInt(student.getCurrentYearOfStudy().toString()));
        studentDto.setEmail(student.getEmail());
        studentDto.setFirstName(student.getFirstName());
        studentDto.setLastName(student.getLastName());
        studentDto.setIndexNumber(student.getIndexNumber());
        studentDto.setCity(student.getCity().getCity());
        studentDto.setPhone(student.getPhoneNumber());
        return studentDto;
    }

    public Set<StudentDto> convertToDtoSet(Set<Student> students) {
        Set<StudentDto> studentDtoSet = students.stream().map(student -> {
            StudentDto studentDto = convertToDto(student);
            return studentDto;
        }).collect(Collectors.toSet());
        return studentDtoSet;
    }
}
