package zarko.trampic.fon.converter;

import org.springframework.stereotype.Service;
import zarko.trampic.fon.dto.SubjectDto;
import zarko.trampic.fon.entity.Subject;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SubjectConverter {

    public Subject convertToEntity(SubjectDto subjectDto) {
        Subject subject = new Subject();
        subject.setDescription(subjectDto.getDescription());
        subject.setSemester(subjectDto.getSemester());
        subject.setYearOfStudy(subjectDto.getYearOfStudy());
        subject.setSubjectName(subjectDto.getSubjectName());
        if(subjectDto.getIdSubject() != null) subject.setIdSubject(subjectDto.getIdSubject());
        return subject;
    }

    public SubjectDto convertToDto(Subject subject){
        SubjectDto subjectDto = new SubjectDto();
        subjectDto.setDescription(subject.getDescription());
        subjectDto.setIdSubject(subject.getIdSubject());
        subjectDto.setSemester(subject.getSemester());
        subjectDto.setSubjectName(subject.getSubjectName());
        subjectDto.setYearOfStudy(subject.getYearOfStudy());
        return subjectDto;
    }

    public Set<SubjectDto> convertToDtoSet(Set<Subject> subjectSet) {
        Set<SubjectDto> subjectDtos = subjectSet.stream().map(subject -> {
            SubjectDto subjectDto = convertToDto(subject);
            return subjectDto;
        }).collect(Collectors.toSet());
        return subjectDtos;
    }
}
