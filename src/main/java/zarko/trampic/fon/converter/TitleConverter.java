package zarko.trampic.fon.converter;

import org.springframework.stereotype.Service;
import zarko.trampic.fon.entity.Title;
import zarko.trampic.fon.enums.TitleName;

@Service
public class TitleConverter {

    public String convertToDto(Title title) {
        String titleDto = title.getTitleName().toString();
        return titleDto;
    }
    public Title convertToEntity(String title) {
        Title newTitle = new Title();
        switch (title){
            case "professor":
                newTitle.setTitleName(TitleName.PROFESSOR);
                return newTitle;
            case "assistant":
                newTitle.setTitleName(TitleName.ASSISTANT_PROFESSOR);
                return newTitle;
            default:
                newTitle.setTitleName(TitleName.ASSOCIATE_PROFESSOR);
                return  newTitle;
        }
    }
}
