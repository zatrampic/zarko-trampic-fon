package zarko.trampic.fon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import zarko.trampic.fon.dto.UserDto;
import zarko.trampic.fon.entity.User;

@Service
public class UserConverter {
    private final RoleConverter roleConverter;
    private final BCryptPasswordEncoder encoder;
    @Autowired
    public UserConverter(RoleConverter roleConverter, BCryptPasswordEncoder encoder) {
        this.roleConverter = roleConverter;
        this.encoder = encoder;
    }

    public User convertToEntity(UserDto userDto) {
        User user = new User()
              .setUserName(userDto.getUsername()).setLastName(userDto.getLastName())
              .setPassword(encoder.encode(userDto.getPassword()))
              .setfirstName(userDto.getFirstName()).setRoles(userDto.getAppRoles())
            .build();
        return user;
    }

    public Object convertToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setUsername(user.getUserName());
        userDto.setAppRoles(user.getAppRoles());
        return user;
    }
}
