package zarko.trampic.fon.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ExamDto {
    private Long idExam;
    @NotNull(message = "Pick professor from select.")
    private Long idProfessor;
    @NotNull(message = "Pick subject from select.")
    private Long idSubject;
    @NotBlank
    @NotNull(message = "Pick date please.")
    private String dateOfExam;
    private String expired;
    public Long getIdExam() {
        return idExam;
    }

    public void setIdExam(Long idExam) {
        this.idExam = idExam;
    }

    public Long getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Long idProfessor) {
        this.idProfessor = idProfessor;
    }

    public Long getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(Long idSubject) {
        this.idSubject = idSubject;
    }

    public String getDateOfExam() {
        return dateOfExam;
    }

    public void setDateOfExam(String dateOfExam) {
        this.dateOfExam = dateOfExam;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }
}
