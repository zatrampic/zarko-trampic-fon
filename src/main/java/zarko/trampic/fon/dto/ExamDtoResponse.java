package zarko.trampic.fon.dto;

import java.util.HashSet;
import java.util.Set;

public class ExamDtoResponse {
    private Long idExamDtoResponse;
    private ProfessorDto professorDto;
    private SubjectDto subjectDto;
    private String dateOfExam;
    private String expired;
    private Set<ExamRegistrationDtoRequest> setRegistrations = new HashSet<>();

    public Long getIdExamDtoResponse() {
        return idExamDtoResponse;
    }

    public void setIdExamDtoResponse(Long idExamDtoResponse) {
        this.idExamDtoResponse = idExamDtoResponse;
    }

    public ProfessorDto getProfessorDto() {
        return professorDto;
    }

    public void setProfessorDto(ProfessorDto professorDto) {
        this.professorDto = professorDto;
    }

    public SubjectDto getSubjectDto() {
        return subjectDto;
    }

    public void setSubjectDto(SubjectDto subjectDto) {
        this.subjectDto = subjectDto;
    }

    public String getDateOfExam() {
        return dateOfExam;
    }

    public void setDateOfExam(String dateOfExam) {
        this.dateOfExam = dateOfExam;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public Set<ExamRegistrationDtoRequest> getSetRegistrations() {
        return setRegistrations;
    }

    public void setSetRegistrations(Set<ExamRegistrationDtoRequest> setRegistrations) {
        this.setRegistrations = setRegistrations;
    }
}
