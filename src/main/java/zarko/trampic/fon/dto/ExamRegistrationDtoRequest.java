package zarko.trampic.fon.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ExamRegistrationDtoRequest {
    @Size(min = 10, max = 10)
    private String studentIndex;
    private Long examId;
    @NotBlank(message = "Not blank")
    private String dateOfRegistration;

    public String getStudentIndex() {
        return studentIndex;
    }

    public void setStudentIndex(String studentIndex) {
        this.studentIndex = studentIndex;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(String dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }
}
