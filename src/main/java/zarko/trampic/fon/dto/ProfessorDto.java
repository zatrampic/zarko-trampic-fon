package zarko.trampic.fon.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ProfessorDto {
    private Long id;
    @Size(min = 3, max = 30, message = "Minimum letters is 3, maximum 30.")
    @NotBlank(message = "Not blank.")
    private String firstName;
    @Size(min = 3, max = 30, message = "Minimum letters is 3, maximum 30.")
    @NotBlank(message = "Not blank.")
    private String lastName;
    @Email(message="Please provide a valid email address")
    @NotBlank(message="Please provide a valid email address")
    private String email;
    @Size(min = 3, message = "Minimum letters is 3.")
    private String address;
    private String city;
    @Size(min = 6,max = 15, message = "Minimum letters is 6, maximum 15")
    private String phone;
    @NotBlank(message = "Not blank.")
    private String reelectionDate;
    @NotBlank(message = "Not blank.")
    @Pattern(regexp = "Professor|Assistant|Associate")
    private String title;

    public ProfessorDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReelectionDate() {
        return reelectionDate;
    }

    public void setReelectionDate(String reelectionDate) {
        this.reelectionDate = reelectionDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
