package zarko.trampic.fon.dto;

import javax.validation.constraints.*;

public class StudentDto {
    private Long idStudent;
    @Size(min = 10, max = 10)
    @NotBlank(message = "Not blank.")
    private String indexNumber;
    @Size(min = 3, max = 30, message = "Minimum letters is 3, maximum 30.")
    @NotBlank(message = "Not blank.")
    private String firstName;
    @Size(min = 3, max = 30, message = "Minimum letters is 3, maximum 30.")
    @NotBlank(message = "Not blank.")
    private String lastName;
    @Email(message="Please provide a valid email address")
    @NotBlank(message="Please provide a valid email address")
    private String email;
    @Size(min = 3, max = 50, message = "Minimum letters is 3, maximum 50.")
    private String address;
    @NotBlank(message = "Not blank.")
    private String city;
    @Size(min = 6, max = 15, message = "Minimum letters is 6, maximum 15.")
    private String phone;
    @Min(value=1, message="must be equal or greater than 1")
    @Max(value=7, message="must be equal or less than 7")
    private int currentYearOfStudy;

    public StudentDto() {
    }

    public Long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Long idStudent) {
        this.idStudent = idStudent;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCurrentYearOfStudy() {
        return currentYearOfStudy;
    }

    public void setCurrentYearOfStudy(int currentYearOfStudy) {
        this.currentYearOfStudy = currentYearOfStudy;
    }
}
