package zarko.trampic.fon.dto;

import zarko.trampic.fon.enums.Semester;

import javax.validation.constraints.*;

public class SubjectDto {
    private Long idSubject;
    @Size(min = 3, max = 30, message = "Minimum letters is 3, maximum 30.")
    @NotBlank(message = "Not blank.")
    private String subjectName;
    private String description;
    @Min(value=1, message="must be equal or greater than 1")
    @Max(value=7, message="must be equal or less than 7")
    private int yearOfStudy;
    @Pattern(regexp = "Summer|Winter")
    private String semester;

    public Long getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(Long idSubject) {
        this.idSubject = idSubject;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
