package zarko.trampic.fon.dto;

import zarko.trampic.fon.entity.AppRole;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

public class UserDto {
    private Long id;
    @NotBlank(message = "Not blank.")
    @Size(min = 4, max = 30, message = "Min 4 letters, max 30.")
    private String username;
    @NotBlank(message = "Not blank.")
    @Size(min = 4, max = 30, message = "Min 4 letters, max 30.")
    private String password;
    @NotBlank(message = "Not blank.")
    @Size(min = 4, max = 30, message = "Min 4 letters, max 30.")
    private String firstName;
    @NotBlank(message = "Not blank.")
    @Size(min = 4, max = 30, message = "Min 4 letters, max 30.")
    private String lastName;
    private Set<AppRole> appRoles;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<AppRole> getAppRoles() {
        return appRoles;
    }

    public void setAppRoles(Set<AppRole> appRoles) {
        this.appRoles = appRoles;
    }
}
