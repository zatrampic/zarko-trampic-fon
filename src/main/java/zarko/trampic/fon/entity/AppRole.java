package zarko.trampic.fon.entity;

import zarko.trampic.fon.enums.RoleName;

import javax.persistence.*;

@Entity
public class AppRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "enum('ROLE_ADMIN','ROLE_USER')")
    @Enumerated(value = EnumType.STRING)
    RoleName roleName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleName getRoleName() {
        return roleName;
    }

    public void setRoleName(RoleName roleName) {
        this.roleName = roleName;
    }
}
