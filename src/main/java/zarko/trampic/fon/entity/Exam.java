package zarko.trampic.fon.entity;

import zarko.trampic.fon.repository.ExamRepository;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Exam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idExam;
    @ManyToOne(fetch = FetchType.LAZY)
    private Subject subject;
    @ManyToOne(fetch = FetchType.LAZY)
    private Professor professor;
    @Temporal(TemporalType.DATE)
    private Date examDate;
    private boolean expired;
    @OneToMany(
            mappedBy = "exam",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<ExamRegistration> examRegistrations = new HashSet<>();

    public Set<ExamRegistration> getExamRegistrations() {
        return examRegistrations;
    }

    public void setExamRegistrations(Set<ExamRegistration> examRegistrations) {
        this.examRegistrations = examRegistrations;
    }

    public Long getIdExam() {
        return idExam;
    }

    public void setIdExam(Long idExam) {
        this.idExam = idExam;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Exam)) return false;
        Exam exam = (Exam) o;
        return Objects.equals(idExam, exam.idExam);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    /*
    ------------Methods for synchronization------------
     */
    public void addExamRegistration(ExamRegistration examRegistration){
        examRegistrations.add(examRegistration);
        examRegistration.setExam(this);
    }
    public void removeExamRegistration(ExamRegistration examRegistration){
        examRegistrations.remove(examRegistration);
        examRegistration.setExam(null);
    }
}
