package zarko.trampic.fon.entity;

import javax.persistence.*;
import java.util.Date;
@Entity
public class ExamRegistration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idExamRegistration;
    private String studentIndex;
    @ManyToOne(fetch = FetchType.LAZY)
    private Exam exam;
    private Date dateOfRegistration;

    public Long getIdExamRegistration() {
        return idExamRegistration;
    }

    public void setIdExamRegistration(Long idExamRegistration) {
        this.idExamRegistration = idExamRegistration;
    }

    public String getStudentIndex() {
        return studentIndex;
    }

    public void setStudentIndex(String studentIndex) {
        this.studentIndex = studentIndex;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }
}
