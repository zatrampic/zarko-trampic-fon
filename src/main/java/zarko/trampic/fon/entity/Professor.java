package zarko.trampic.fon.entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProfessor;
    @Size(min = 3, max = 30)
    @NotNull
    private String firstName;
    @Size(min = 3, max = 30)
    @NotNull
    private String lastName;
    @Email
    @Column(unique = true)
    private String email;
    @Size(min = 3)
    private String address;
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinColumn(
            name = "city",
            referencedColumnName = "id"
    )
    private Place city;
    @Size(min = 6)
    private String phoneNumber;
    @NotNull
    @Basic
    @Temporal(TemporalType.DATE)
    private Date reelectionDate;
    @NotNull
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinColumn(
            name = "title",
            referencedColumnName = "idTitle"
    )
    private Title title;
    @OneToMany(
            mappedBy = "professor",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Exam> exams = new HashSet<>();

    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

    public Long getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Long idProfessor) {
        this.idProfessor = idProfessor;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Place getCity() {
        return city;
    }

    public void setCity(Place city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getReelectionDate() {
        return reelectionDate;
    }

    public void setReelectionDate(Date reelectionDate) {
        this.reelectionDate = reelectionDate;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }
    /*
        -------- methods for synchronization----------
         */
    public void addExam(Exam exam){
        exams.add(exam);
        exam.setProfessor(this);
    }
    public void remove(Exam exam){
        exams.remove(exam);
        exam.setProfessor(null);
    }
}
