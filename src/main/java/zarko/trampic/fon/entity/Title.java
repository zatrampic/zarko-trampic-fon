package zarko.trampic.fon.entity;

import org.hibernate.annotations.NaturalId;
import zarko.trampic.fon.enums.TitleName;

import javax.persistence.*;

@Entity
public class Title {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTitle;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, unique = true)
    private TitleName titleName;

    public Long getIdTitle() {
        return idTitle;
    }

    public void setIdTitle(Long idTitle) {
        this.idTitle = idTitle;
    }

    public TitleName getTitleName() {
        return titleName;
    }

    public void setTitleName(TitleName titleName) {
        this.titleName = titleName;
    }
}
