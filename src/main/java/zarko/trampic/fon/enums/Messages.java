package zarko.trampic.fon.enums;

public enum Messages {
    REGISTRATION_SUCCESS ("Success registration. You can now login."),
    USER_ALREADY_EXIST ("User with that username already exist."),
    USER_NOT_FOUND ("User not found!"),
    PROFESSOR_ADD_SUCCESS("Success inserting new professor."),
    PROFESSOR_ADD_FAILED ("Inserting professor failed. Email in use."),
    BAD_CREDENTIALS ("Bad Credentials."),
    DELETE_PROFESSOR_SUCCESS ("Success deleting professor."),
    PROFESSOR_UPDATE_SUCCESS ("Succes updating professor"),
    PROFESSOR_UPDATE_FAILED ("Updating professor failed."),
    STUDENT_INSERT_FAILED ("Inserting student failed. Email in use."),
    STUDENT_INSERT_SUCCESS ("Success inserting new student."),
    DELETE_STUDENT_SUCCESS ("Success deleting student."),
    SUBJECT_INSERT_FAILED ("Inserting subject failed."),
    SUBJECT_INSERT_SUCCESS ("Success inserting new subject."),
    DELETE_SUBJECT_SUCCESS ("Success deleting subject"),
    EXAM_INSERT_SUCCESS ("Succes inserting new exam."),
    EXAM_INSERT_FAILED ("Inserting new exam failed."),
    INSERT_REGISTRATION_EXAM_SUCCES ("Success! All conditions satisfied."),
    NO_SUCH_STUDENT ("No such student with that index."),
    REG_EXAM_NOT_VALID ("Registration date isn`n valid."),
    EXAM_YEAR_NOT_VALID ("Exam year is after current year of student!"),
    SOMETHING_WENT_WRONG ("Something went wrong.");


    private final String message;
    Messages(String s) {
        message = s;
    }

    public String getMessage(){
        return this.message;
    }
}
