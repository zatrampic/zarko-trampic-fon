package zarko.trampic.fon.enums;

public enum TitleName {
    ASSISTANT_PROFESSOR,ASSOCIATE_PROFESSOR,PROFESSOR
}
