package zarko.trampic.fon.exceptions;

public class MyDateNotValidException extends Exception{
    public MyDateNotValidException() {
    }

    public MyDateNotValidException(String message) {
        super(message);
    }

    public MyDateNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyDateNotValidException(Throwable cause) {
        super(cause);
    }

    public MyDateNotValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
