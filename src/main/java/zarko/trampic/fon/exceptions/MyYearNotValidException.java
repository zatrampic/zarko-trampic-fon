package zarko.trampic.fon.exceptions;

public class MyYearNotValidException extends Exception{

    public MyYearNotValidException() {
    }

    public MyYearNotValidException(String message) {
        super(message);
    }

    public MyYearNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyYearNotValidException(Throwable cause) {
        super(cause);
    }

    public MyYearNotValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
