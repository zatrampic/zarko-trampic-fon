package zarko.trampic.fon.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zarko.trampic.fon.converter.ExamConverter;
import zarko.trampic.fon.dto.ExamDto;
import zarko.trampic.fon.dto.ExamDtoResponse;
import zarko.trampic.fon.entity.Exam;
import zarko.trampic.fon.service.ExamService;

import java.text.ParseException;
import java.util.Set;

@Component
public class ExamFacade {
    private final ExamService examService;
    private final ExamConverter examConverter;
    @Autowired
    public ExamFacade(ExamService examService, ExamConverter examConverter) {
        this.examService = examService;
        this.examConverter = examConverter;
    }

    public void insertExam(ExamDto examDto) throws ParseException {
        examService.insertExam(examDto);
    }

    public Set<ExamDtoResponse> findByProfessorId(Long id,Integer pageNumber, Integer pageSize) {
        return examService.findByProfessorId(id,pageNumber,pageSize);
    }

    public Set<ExamDtoResponse> getAll(Integer pageNumber, Integer pageSize) {
        return examService.getAllPaging(pageNumber,pageSize);
    }

    public Set<ExamDtoResponse> findBysubjectId(Long id, Integer pageNumber, Integer pageSize) {
        return examService.findBySubjectId(id,pageNumber,pageSize);
    }
}
