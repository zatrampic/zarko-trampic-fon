package zarko.trampic.fon.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zarko.trampic.fon.converter.ExamRegistrationConverter;
import zarko.trampic.fon.dto.ExamRegistrationDtoRequest;
import zarko.trampic.fon.entity.ExamRegistration;
import zarko.trampic.fon.exceptions.MyDateNotValidException;
import zarko.trampic.fon.exceptions.MyNoSuchElementException;
import zarko.trampic.fon.exceptions.MyYearNotValidException;
import zarko.trampic.fon.service.ExamRegistrationService;

import java.text.ParseException;

@Component
public class ExamRegistrationFacade {
    private final ExamRegistrationService examRegistrationService;
    private final ExamRegistrationConverter examRegistrationConverter;
    @Autowired
    public ExamRegistrationFacade(ExamRegistrationService examRegistrationService, ExamRegistrationConverter examRegistrationConverter) {
        this.examRegistrationService = examRegistrationService;
        this.examRegistrationConverter = examRegistrationConverter;
    }

    public void insertExamRegistration(ExamRegistrationDtoRequest examRegistrationDtoRequest) throws ParseException, MyDateNotValidException, MyYearNotValidException, MyNoSuchElementException {
        ExamRegistration newExamRegistration = examRegistrationConverter.convertToEntity(examRegistrationDtoRequest);
        examRegistrationService.insertNew(newExamRegistration);
    }
}
