package zarko.trampic.fon.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zarko.trampic.fon.converter.ProfessorConverter;
import zarko.trampic.fon.dto.ProfessorDto;
import zarko.trampic.fon.entity.Professor;
import zarko.trampic.fon.service.ProfessorService;
import java.util.Set;

@Component
public class ProfessorFacade {
    private final ProfessorService professorService;
    private final ProfessorConverter professorConverter;
    @Autowired
    public ProfessorFacade(ProfessorService professorService, ProfessorConverter professorConverter) {
        this.professorService = professorService;
        this.professorConverter = professorConverter;
    }

    public void insertNew(ProfessorDto professorDto) throws Exception {
        try{
            Professor newProfessor = professorConverter.convertToEntity(professorDto);
            Professor insertedProf = professorService.insertNew(newProfessor);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    public Set<ProfessorDto> getAll() {
        Set<Professor> professorSet = professorService.getAll();
        Set<ProfessorDto> dtos = professorConverter.convertToDTOsSet(professorSet);
        return dtos;
    }

    public void deleteById(Long id) {
        professorService.deleteById(id);
    }

    public void update(ProfessorDto professorDto) throws Exception {
            Professor professor = professorConverter.convertToEntity(professorDto);
            professorService.update(professor);
    }
}
