package zarko.trampic.fon.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zarko.trampic.fon.converter.StudentConverter;
import zarko.trampic.fon.dto.StudentDto;
import zarko.trampic.fon.entity.Student;
import zarko.trampic.fon.service.StudentService;

import java.util.Set;

@Component
public class StudentFacade {
    private final StudentService studentService;
    private final StudentConverter studentConverter;
    @Autowired
    public StudentFacade(StudentService studentService, StudentConverter studentConverter) {
        this.studentService = studentService;
        this.studentConverter = studentConverter;
    }

    public void insertNewStudent(StudentDto studentDto) {
        Student newStudent = studentConverter.convertToEntity(studentDto);
        studentService.insertNew(newStudent);
    }

    public Set<StudentDto> getAll() {
        Set<Student> students = studentService.getAll();
        Set<StudentDto> studentDtoSet = studentConverter.convertToDtoSet(students);
        return studentDtoSet;
    }

    public void deleteById(Long id) {
        studentService.deleteById(id);
    }
}
