package zarko.trampic.fon.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zarko.trampic.fon.converter.SubjectConverter;
import zarko.trampic.fon.dto.SubjectDto;
import zarko.trampic.fon.entity.Subject;
import zarko.trampic.fon.service.SubjectService;

import java.util.Set;

@Component
public class SubjectFacade {
    private final SubjectService subjectService;
    private final SubjectConverter subjectConverter;
    @Autowired
    public SubjectFacade(SubjectService subjectService, SubjectConverter subjectConverter) {
        this.subjectService = subjectService;
        this.subjectConverter = subjectConverter;
    }

    public void insertNew(SubjectDto subjectDto) {
        Subject newSubject = subjectConverter.convertToEntity(subjectDto);
        subjectService.insertNew(newSubject);
    }

    public Set<SubjectDto> getAll() {
        Set<Subject> subjectSet = subjectService.getAll();
        Set<SubjectDto> subjectDtos = subjectConverter.convertToDtoSet(subjectSet);
        return subjectDtos;
    }

    public void deleteById(Long id) {
        subjectService.deleteById(id);
    }
}
