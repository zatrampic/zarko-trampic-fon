package zarko.trampic.fon.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zarko.trampic.fon.converter.UserConverter;
import zarko.trampic.fon.dto.UserDto;
import zarko.trampic.fon.entity.AppRole;
import zarko.trampic.fon.entity.User;
import zarko.trampic.fon.service.RoleService;
import zarko.trampic.fon.service.UserService;

import java.util.Set;

@Component
public class UserFacade {
    private final UserService userService;
    private final UserConverter userConverter;
    private final RoleService roleService;
    @Autowired
    public UserFacade(UserService userService, UserConverter userConverter, RoleService roleService) {
        this.userService = userService;
        this.userConverter = userConverter;
        this.roleService = roleService;
    }

    public void saveNewUser(UserDto userDto) throws Exception {
        Set<AppRole> set = roleService.getAll();
        userDto.setAppRoles(set);
        User newUser = userConverter.convertToEntity(userDto);
        try{
            userService.saveNewUser(newUser);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
