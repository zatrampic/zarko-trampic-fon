package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.ExamRegistration;

@Repository
public interface ExamRegistrationRepository extends JpaRepository<ExamRegistration,Long> {
}
