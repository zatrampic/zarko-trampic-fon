package zarko.trampic.fon.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.Exam;
import java.util.Set;

@Repository
public interface ExamRepository extends JpaRepository<Exam,Long>, PagingAndSortingRepository<Exam,Long>{
    @Query(value = "select * from exam where professor_idProfessor = ?",nativeQuery = true)
    Set<Exam> findAllByProfessorID(Long id, PageRequest paging);
    @Query(value = "select * from exam where subject_idSubject = ?",nativeQuery = true)
    Set<Exam> findAllBySubjectID(Long id, PageRequest paging);
}
