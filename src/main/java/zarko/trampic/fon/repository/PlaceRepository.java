package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.Place;
@Repository
public interface PlaceRepository extends JpaRepository<Place,Long> {
    @Query(value = "sELECT * FROM `place` WHERE city LIKE ?",nativeQuery = true)
    Place findByCityName(String city);
}
