package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.Professor;
@Repository
public interface ProfessorRepository extends JpaRepository<Professor,Long>{
}
