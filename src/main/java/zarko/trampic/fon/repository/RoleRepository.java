package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.AppRole;

import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<AppRole,Long> {
    @Query(value = "select * from approle",nativeQuery = true)
    Set<AppRole> getAll();
}
