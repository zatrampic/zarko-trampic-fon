package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
    @Query(value = "SELECT * FROM `student` WHERE `indexNumber` = ?",nativeQuery = true)
    Student findByIndex(String studentIndex);
}
