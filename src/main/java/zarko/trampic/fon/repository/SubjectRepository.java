package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.Subject;
@Repository
public interface SubjectRepository extends JpaRepository<Subject,Long> {
}
