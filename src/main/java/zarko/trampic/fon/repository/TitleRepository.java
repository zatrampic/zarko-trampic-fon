package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.Title;
@Repository
public interface TitleRepository extends JpaRepository<Title,Long> {
    @Query(value = "select * from title where titleName LIKE ?",nativeQuery = true)
    Title findByTitleName(String title);
}
