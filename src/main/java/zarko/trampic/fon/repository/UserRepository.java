package zarko.trampic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import zarko.trampic.fon.entity.User;
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    @Query(value = "select * from user where userName LIKE ?",nativeQuery = true)
    User findByUsername(String username);
}
