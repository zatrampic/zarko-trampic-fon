package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.ExamRegistration;
import zarko.trampic.fon.exceptions.MyDateNotValidException;
import zarko.trampic.fon.exceptions.MyNoSuchElementException;
import zarko.trampic.fon.exceptions.MyYearNotValidException;

public interface ExamRegistrationService {
    void insertNew(ExamRegistration newExamRegistration) throws MyDateNotValidException, MyYearNotValidException, MyNoSuchElementException;
}
