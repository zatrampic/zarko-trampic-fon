package zarko.trampic.fon.service;

import zarko.trampic.fon.dto.ExamDto;
import zarko.trampic.fon.dto.ExamDtoResponse;
import java.text.ParseException;
import java.util.Set;

public interface ExamService {
    void insertExam(ExamDto examDto) throws ParseException;
    Set<ExamDtoResponse> findByProfessorId(Long id,Integer pageNumber, Integer pageSize);
    Set<ExamDtoResponse> getAllPaging(Integer pageNumber, Integer pageSize);
    Set<ExamDtoResponse> findBySubjectId(Long id, Integer pageNumber, Integer pageSize);
}
