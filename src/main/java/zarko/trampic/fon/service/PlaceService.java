package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.Place;

public interface PlaceService {
    Place isExist(String city) throws Exception;
    Place insertPlace(Place place) throws Exception;
}
