package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.Professor;

import java.util.Set;

public interface ProfessorService {
    Professor insertNew(Professor newProfessor);
    Set<Professor> getAll();
    void deleteById(Long id);
    void update(Professor professor);
    Professor findById(Long id);
}
