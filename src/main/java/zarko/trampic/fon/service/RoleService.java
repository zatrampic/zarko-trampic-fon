package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.AppRole;

import java.util.Set;

public interface RoleService {
    Set<AppRole> getAll();
}
