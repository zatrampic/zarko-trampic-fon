package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.Student;

import java.util.Set;

public interface StudentService {
    void insertNew(Student student);
    Set<Student> getAll();
    void deleteById(Long id);
}
