package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.Subject;

import java.util.Set;

public interface SubjectService {
    void insertNew(Subject newSubject);
    Set<Subject> getAll();
    void deleteById(Long id);
}
