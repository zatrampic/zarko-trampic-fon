package zarko.trampic.fon.service;

import zarko.trampic.fon.entity.User;

public interface UserService {
    void saveNewUser(User newUser);
}
