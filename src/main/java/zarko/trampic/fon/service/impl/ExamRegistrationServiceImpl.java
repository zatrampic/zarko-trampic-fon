package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.plugin2.message.Message;
import zarko.trampic.fon.entity.Exam;
import zarko.trampic.fon.entity.ExamRegistration;
import zarko.trampic.fon.entity.Student;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.exceptions.MyDateNotValidException;
import zarko.trampic.fon.exceptions.MyNoSuchElementException;
import zarko.trampic.fon.exceptions.MyYearNotValidException;
import zarko.trampic.fon.repository.ExamRegistrationRepository;
import zarko.trampic.fon.repository.ExamRepository;
import zarko.trampic.fon.repository.StudentRepository;
import zarko.trampic.fon.service.ExamRegistrationService;

import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;

@Service
public class ExamRegistrationServiceImpl implements ExamRegistrationService {
    private final ExamRegistrationRepository examRegistrationRepository;
    private final ExamRepository examRepository;
    private final StudentRepository studentRepository;
    @Autowired
    public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository, ExamRepository examRepository, StudentRepository studentRepository) {
        this.examRegistrationRepository = examRegistrationRepository;
        this.examRepository = examRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    @Transactional
    public void insertNew(ExamRegistration examRegistration) throws MyDateNotValidException, MyYearNotValidException, MyNoSuchElementException {
           Exam foundExam = examRepository.findById(examRegistration.getExam().getIdExam()).get();
           Student foundStudent = studentRepository.findByIndex(examRegistration.getStudentIndex());
           if(foundStudent == null) throw new MyNoSuchElementException(Messages.NO_SUCH_STUDENT.getMessage());
           if(foundExam != null){
               boolean isDateValid = checkIfDateIsValid(examRegistration.getDateOfRegistration(), foundExam.getExamDate());
               if(isDateValid == false) throw new MyDateNotValidException(Messages.REG_EXAM_NOT_VALID.getMessage());
           }
           if(foundStudent != null){
               boolean isYearValid = checkIfYearIsValid(foundStudent,foundExam);
               if(isYearValid == false) throw new MyYearNotValidException(Messages.EXAM_YEAR_NOT_VALID.getMessage());
           }
           examRegistration.setExam(foundExam);
           examRegistrationRepository.save(examRegistration);
    }


    private boolean checkIfYearIsValid(Student foundStudent, Exam foundExam) {
        int result = foundExam.getSubject().getYearOfStudy()-Integer.parseInt(foundStudent.getCurrentYearOfStudy().toString());
        if(result <= 0) return true;
        return false;
    }

    private boolean checkIfDateIsValid(Date dateOfRegistration, Date examDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(examDate);
        calendar.add(Calendar.WEEK_OF_YEAR,-1);
        Date weekBefore = calendar.getTime();
        return dateOfRegistration.after(weekBefore) && dateOfRegistration.before(examDate);
    }
}
