package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.converter.ExamConverter;
import zarko.trampic.fon.dto.ExamDto;
import zarko.trampic.fon.dto.ExamDtoResponse;
import zarko.trampic.fon.entity.Exam;
import zarko.trampic.fon.entity.Professor;
import zarko.trampic.fon.entity.Subject;
import zarko.trampic.fon.repository.ExamRepository;
import zarko.trampic.fon.repository.ProfessorRepository;
import zarko.trampic.fon.repository.SubjectRepository;
import zarko.trampic.fon.service.ExamService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@Service
public class ExamServiceImpl implements ExamService {
    private final ExamRepository examRepository;
    private final SubjectRepository subjectRepository;
    private final ProfessorRepository professorRepository;
    private final ExamConverter examConverter;
    @Autowired
    public ExamServiceImpl(ExamRepository examRepository, SubjectRepository subjectRepository, ProfessorRepository professorRepository, ExamConverter examConverter) {
        this.examRepository = examRepository;
        this.subjectRepository = subjectRepository;
        this.professorRepository = professorRepository;
        this.examConverter = examConverter;
    }

    @Override
    @Transactional
    public void insertExam(ExamDto examDto) throws ParseException {
        Exam newExam = new Exam();
        Professor foundProfessor = professorRepository.findById(examDto.getIdProfessor()).get();
        if(foundProfessor != null) newExam.setProfessor(foundProfessor);
        Subject foundSubject = subjectRepository.findById(examDto.getIdSubject()).get();
        if(foundSubject != null) newExam.setSubject(foundSubject);
        newExam.setExamDate(convertToDate(examDto.getDateOfExam()));
        Exam insertedExam = examRepository.save(newExam);
        foundProfessor.addExam(insertedExam);
        foundSubject.addExam(insertedExam);
    }

    @Override
    @Transactional
    public Set<ExamDtoResponse> findByProfessorId(Long id, Integer pageNumber, Integer pageSize) {
        PageRequest paging = PageRequest.of(pageNumber,pageSize);
        Set<Exam> pageResults = examRepository.findAllByProfessorID(id,paging);
        return examConverter.convertToExamDtoResponseSet(pageResults);
    }

    @Override
    @Transactional
    public Set<ExamDtoResponse> getAllPaging(Integer pageNumber, Integer pageSize) {
        PageRequest paging = PageRequest.of(pageNumber,pageSize);
        Page<Exam> pagedResults = examRepository.findAll(paging);
        return examConverter.convertToExamDtoResponseSet(pagedResults.toSet());
    }

    @Override
    @Transactional
    public Set<ExamDtoResponse> findBySubjectId(Long id, Integer pageNumber, Integer pageSize) {
        PageRequest paging = PageRequest.of(pageNumber,pageSize);
        Set<Exam> pagedResults = examRepository.findAllBySubjectID(id,paging);
        return examConverter.convertToExamDtoResponseSet(pagedResults);
    }

    private Date convertToDate(String dateOfExam) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.parse(dateOfExam);
    }
}
