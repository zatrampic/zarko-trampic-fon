package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.Place;
import zarko.trampic.fon.repository.PlaceRepository;
import zarko.trampic.fon.service.PlaceService;
@Service
public class PlaceServiceImpl implements PlaceService {
    private final PlaceRepository placeRepository;
    @Autowired
    public PlaceServiceImpl(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }


    @Override
    @Transactional
    public Place insertPlace(Place place) throws Exception {
        try{
            Place newPlace = placeRepository.save(place);
            return newPlace;
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
    @Override
    @Transactional
    public Place isExist(String city) throws Exception {
        Place place = placeRepository.findByCityName(city);
        if(place == null){
            Place newPlace = new Place();
            newPlace.setCity(city);
            newPlace.setPostNumber("10000");
            Place savedPlace = insertPlace(newPlace);
            return savedPlace;
        }else {
            return place;
        }
    }
}
