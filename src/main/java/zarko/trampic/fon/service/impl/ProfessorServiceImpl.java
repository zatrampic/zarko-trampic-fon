package zarko.trampic.fon.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.Place;
import zarko.trampic.fon.entity.Professor;
import zarko.trampic.fon.entity.Title;
import zarko.trampic.fon.repository.PlaceRepository;
import zarko.trampic.fon.repository.ProfessorRepository;
import zarko.trampic.fon.repository.TitleRepository;
import zarko.trampic.fon.service.ProfessorService;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;
    private final TitleRepository titleRepository;
    private final PlaceRepository placeRepository;
    @Autowired
    public ProfessorServiceImpl(ProfessorRepository professorRepository, TitleRepository titleRepository, PlaceRepository placeRepository) {
        this.professorRepository = professorRepository;
        this.titleRepository = titleRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    @Transactional
    public Professor insertNew(Professor professor) {
        Title foundTitle = titleRepository.findByTitleName(professor.getTitle().getTitleName().toString());
        if(foundTitle != null){
            professor.setTitle(foundTitle);
        }else {
            professor.setTitle(titleRepository.save(professor.getTitle()));
        }
        Place foundPlace = placeRepository.findByCityName(professor.getCity().getCity());
        if(foundPlace != null){
            professor.setCity(foundPlace);
        }
        return professorRepository.save(professor);
    }

    @Override
    @Transactional
    public Set<Professor> getAll() {
        Set<Professor> professorSet = professorRepository.findAll().stream().collect(Collectors.toSet());
        return professorSet;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        professorRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Professor findById(Long id) {
        return professorRepository.findById(id).get();
    }

    @Override
    @Transactional
    public void update(Professor professor) {
        Professor foundProfessor = findById(professor.getIdProfessor());
        if(foundProfessor != null){
            foundProfessor.setEmail(professor.getEmail());
            Place foundPlace = placeRepository.findByCityName(professor.getCity().getCity());
            if(foundPlace == null){
                foundProfessor.setCity(professor.getCity());
            }else {
                foundProfessor.setCity(foundPlace);
            }
            Title foundTitle = titleRepository.findByTitleName(professor.getTitle().getTitleName().toString());
            if(foundTitle == null){
                foundProfessor.setTitle(professor.getTitle());
            }else {
                foundProfessor.setTitle(foundTitle);
            }
            foundProfessor.setReelectionDate(professor.getReelectionDate());
            foundProfessor.setPhoneNumber(professor.getPhoneNumber());
            foundProfessor.setAddress(professor.getAddress());
            foundProfessor.setLastName(professor.getLastName());
            foundProfessor.setFirstName(professor.getFirstName());
            foundProfessor.setIdProfessor(professor.getIdProfessor());
            professorRepository.saveAndFlush(foundProfessor);
        }
    }
}
