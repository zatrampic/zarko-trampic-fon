package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.AppRole;
import zarko.trampic.fon.repository.RoleRepository;
import zarko.trampic.fon.service.RoleService;

import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional
    public Set<AppRole> getAll() {
        return roleRepository.getAll();
    }
}
