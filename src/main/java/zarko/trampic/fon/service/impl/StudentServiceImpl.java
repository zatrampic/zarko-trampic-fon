package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.Place;
import zarko.trampic.fon.entity.Student;
import zarko.trampic.fon.repository.PlaceRepository;
import zarko.trampic.fon.repository.StudentRepository;
import zarko.trampic.fon.service.StudentService;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final PlaceRepository placeRepository;
    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, PlaceRepository placeRepository) {
        this.studentRepository = studentRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    @Transactional
    public void insertNew(Student student) {
        Place foundPlace = placeRepository.findByCityName(student.getCity().getCity());
        if(foundPlace != null){
            student.setCity(foundPlace);
        }
        studentRepository.save(student);
    }

    @Override
    @Transactional
    public Set<Student> getAll() {
        Set<Student> students = studentRepository.findAll().stream().collect(Collectors.toSet());
        return students;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }
}
