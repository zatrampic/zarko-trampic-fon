package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.Subject;
import zarko.trampic.fon.repository.SubjectRepository;
import zarko.trampic.fon.service.SubjectService;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    @Autowired
    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    @Transactional
    public void insertNew(Subject subject) {
        subjectRepository.save(subject);
    }

    @Override
    @Transactional
    public Set<Subject> getAll() {
        Set<Subject> subjectSet = subjectRepository.findAll().stream().collect(Collectors.toSet());
        return subjectSet;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        subjectRepository.deleteById(id);
    }
}
