package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.Title;
import zarko.trampic.fon.enums.TitleName;
import zarko.trampic.fon.repository.TitleRepository;
import zarko.trampic.fon.service.TitleService;
@Service
public class TitleServiceImpl implements TitleService {
    private final TitleRepository titleRepository;
    @Autowired
    public TitleServiceImpl(TitleRepository titleRepository) {
        this.titleRepository = titleRepository;
    }

    @Override
    @Transactional
    public Title insertTitle(Title title) {
        return titleRepository.save(title);
    }
    @Override
    @Transactional
    public Title isExist(String title) {
        Title foundTitle = titleRepository.findByTitleName(title);
        if(foundTitle == null){
            Title newTitle = new Title();
            newTitle.setTitleName(TitleName.PROFESSOR);
            return insertTitle(newTitle);
        }else {
            return foundTitle;
        }
    }
}
