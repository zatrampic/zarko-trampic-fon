package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.converter.UserConverter;
import zarko.trampic.fon.entity.User;
import zarko.trampic.fon.enums.Messages;
import zarko.trampic.fon.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service("UserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    private final UserConverter userConverter;
    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user != null){
            Collection<GrantedAuthority> authorities = user.getAppRoles().stream().map(role->
                    new SimpleGrantedAuthority(role.getRoleName().toString())).collect(Collectors.toCollection(ArrayList::new));
            return new org.springframework.security.core.userdetails.User(user.getUserName(),user.getPassword(),true,true,true,true,authorities);
        }else {
            throw new UsernameNotFoundException(Messages.USER_NOT_FOUND.getMessage());
        }
    }
}
