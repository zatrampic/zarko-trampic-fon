package zarko.trampic.fon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zarko.trampic.fon.entity.User;
import zarko.trampic.fon.repository.UserRepository;
import zarko.trampic.fon.service.UserService;
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Transactional
    public void saveNewUser(User newUser) {
        userRepository.save(newUser);
    }
}
