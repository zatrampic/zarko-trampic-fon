<%@ taglib prefix="th" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}#" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css">
    <script src="${pageContext.request.contextPath}/resources/admin.js"></script>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
          media="screen"
          th:href="@{${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css}"/>
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"
            th:src="@{${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js}"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>

     function setValuesRegistration(idExamForRefitration, subjec, profa) {
        console.log("ID EXAM", subjec);
        var date = new Date();
        var fullDate = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
        document.getElementById("dateForRegistration").value = fullDate;
        document.getElementById('idExamRegistration').value = idExamForRefitration;
         document.getElementById('paragrafSubject').innerHTML = subjec;
         document.getElementById('paragrafProfessor').innerHTML = profa;
    }

    //-------pagination withou library---------
     function getAllExams(pageNumber) {
        var optionSize = document.getElementById('pageSize');
        var pageSize = optionSize.options[optionSize.selectedIndex].value;
        var getAllExamURI = "${pageContext.request.contextPath}/exam/getAll/"+pageSize+"/"+pageNumber;
        window.location.href = getAllExamURI;
    }
    function initExamTab() {
       var initExamURI = "${pageContext.request.contextPath}/exam/initExam";
       window.location.href = initExamURI;
    }
    function searchBySubjectId() {
        var idFromSelect = document.getElementById('subjectDrop');
        var idForLink = idFromSelect.options[idFromSelect.selectedIndex].value;
        var finalURI = "${pageContext.request.contextPath}/exam/examId/"+idForLink;
        window.location.href = finalURI;
    }
    function searchByProfessorId() {
        var idFromSelect = document.getElementById('professorDrop');
        var idForLink = idFromSelect.options[idFromSelect.selectedIndex].value;
        var finalURI = "${pageContext.request.contextPath}/exam/profId/"+idForLink;
        window.location.href = finalURI;
    }
    $(document).ready(function () {
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>
</head>
<body>
<div class="container">
    <div class="logout-end">
        <h3>Admin panel</h3>
        <c:url value="/logout" var="logoutUrl" />
        <form id="logout" action="${logoutUrl}" method="post" >
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <a href="javascript:document.getElementById('logout').submit()">Logout</a>
        </c:if>
        <a href="${pageContext.request.contextPath}/">Landing Page</a>
    </div>
    <br>
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#home">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu1">Professors</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu2">Students</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu3">Subjects</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#menu4" onclick="initExamTab()">
                Exams</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="home" class="container tab-pane active"><br>
            <h3>Final project from Engineering course leaded by team from FON</h3>
            <p>-Dusan Savic</p>
            <p>-Voijslav Stanojevic</p>
            <p>-Ilija Antovic</p>
            <p>-Milos Milic</p>
            <h3>Final project from Engineering course leaded by team from FON</h3>
        </div>
        <div id="menu1" class="container tab-pane fade"><br>
            <div class="my-tab-header">
                <h3 style="color: white">Professors administration</h3>
                <h6 style="color: red">${message}</h6>
            </div>
            <div class="parent">
                <div class="divLeft">
                    <div class="form-wrapper">
                        <form:form action="${pageContext.request.contextPath}/professor/insert" method="post"
                                   modelAttribute="professorDto">
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="firstName">First name</form:label>
                                    <form:input path="firstName" type="text" class="form-control"
                                                placeholder="First name"/>
                                    <form:errors path="firstName" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="lastName">Last name</form:label>
                                    <form:input path="lastName" type="text" class="form-control"
                                                placeholder="Last name"/>
                                    <form:errors path="lastName" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="phone">Phone</form:label>
                                    <form:input path="phone" type="text" class="form-control" placeholder="Phone"/>
                                    <form:errors path="phone" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="address">Address</form:label>
                                    <form:input path="address" type="text" class="form-control" placeholder="Address"/>
                                    <form:errors path="address" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="city">City</form:label>
                                    <form:input path="city" type="text" class="form-control" placeholder="City"/>
                                    <form:errors path="city" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="title">Title</form:label>
                                    <form:input path="title" type="text" class="form-control" placeholder="Title"/>
                                    <form:errors path="title" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="reelectionDate">Reelection date</form:label>
                                    <form:input path="reelectionDate" type="date" class="form-control datepicker"/>
                                    <form:errors path="reelectionDate" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="email">Email</form:label>
                                    <form:input path="email" type="text" class="form-control"/>
                                    <form:errors path="email" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="align-items: center; justify-content: center">
                                <div style="text-align: center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
                <div class="divRight">
                    <div class="row">
                        <div style="text-align: center">
                            <form:form action="${pageContext.request.contextPath}/professor/getAll" method="get">
                                <button type="submit" class="btn btn-primary">Load Professors</button>
                            </form:form>
                        </div>
                    </div>
                    <div class="row">
                        <table class="table table-bordered">
                            <tr>
                                <th>#</th>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Title</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            <c:forEach items="${setProfessors}" var="professorEditDto" varStatus="status">
                                <tr>
                                    <td id="profId">${professorEditDto.id}</td>
                                    <td id="profName">${professorEditDto.firstName}</td>
                                    <td id="profLastName">${professorEditDto.lastName}</td>
                                    <td id="profTitle">${professorEditDto.title}</td>
                                    <td style="display: none" id="profEmail">${professorEditDto.email}</td>
                                    <td style="display: none" id="profDate">${professorEditDto.reelectionDate}</td>
                                    <td style="display: none" id="profPhone">${professorEditDto.phone}</td>
                                    <td style="display: none" id="profCity">${professorEditDto.city}</td>
                                    <td style="display: none" id="profAddress">${professorEditDto.address}</td>
                                    <td>
                                        <button style="font-size: 10px" class="btn btn-outline-primary"
                                                data-toggle="modal" data-target="#edit-professor"
                                                data-professor="${professorEditDto}">
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                        <button style="font-size: 10px" class="btn btn-outline-danger"
                                                data-href="${pageContext.request.contextPath}/professor/delete/${professorEditDto.id}"
                                                data-toggle="modal" data-target="#confirm-delete">
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="edit-professor" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="height: 55vh">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Edit professor</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <form:form id="profForm" action="${pageContext.request.contextPath}/professor/update" modelAttribute="professorDto" method="post">
                                    <div class="row" style="margin-bottom: 1rem">
                                        <div class="col">
                                            <label>First name</label>
                                            <form:input name="editFirstName" path="firstName" id="editFirstName" type="text" class="form-control" value=""/>
                                            <form:errors path="firstName" cssClass="text-danger"/>
                                        </div>
                                        <div class="col">
                                            <label>Last name</label>
                                            <form:input name="editLastName" path="lastName" id="editLastName" type="text" class="form-control"/>
                                            <form:errors path="lastName" cssClass="text-danger"/>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 1rem">
                                        <div class="col">
                                            <label>Address</label>
                                            <form:input name="editAddress" path="address" id="editAddress" type="text" class="form-control"/>
                                            <form:errors path="address" cssClass="text-danger"/>
                                        </div>
                                        <div class="col">
                                            <label>City</label>
                                            <form:input name="editCity" path="city" id="editCity" type="text" class="form-control"/>
                                            <form:errors path="city" cssClass="text-danger"/>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 1rem">
                                        <div class="col">
                                            <label>Email</label>
                                            <form:input name="editEmail" path="email" id="editEmail" type="text" class="form-control"/>
                                            <form:errors path="email" cssClass="text-danger"/>
                                        </div>
                                        <div class="col">
                                            <label>Phone</label>
                                            <form:input name="editPhone" path="phone" id="editPhone" type="text" class="form-control"/>
                                            <form:errors path="phone" cssClass="text-danger"/>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 1rem">
                                        <div class="col">
                                            <label>Reelection date</label>
                                            <form:input name="editDate" path="reelectionDate" id="editDate" type="date" class="form-control"/>
                                            <form:errors path="reelectionDate" cssClass="text-danger"/>
                                        </div>
                                        <div class="col">
                                            <label>Title</label>
                                            <form:input name="editTitle" path="title" id="editTitle" type="text" class="form-control"/>
                                            <form:errors path="title" cssClass="text-danger"/>
                                        </div>
                                    </div>
                                    <form:input name="editId" id="editId" path="id"  type="hidden" style="display: none"/>
                                    <div class="row" style="display: inline-grid;width: 100%;margin: auto;">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function (e) {
                    $('#profForm').on('submit', function () {
                        var editProfDto = {
                            id: $('#editId').val(),
                            firstName: $('#editFirstName').val(),
                            lastName: $('#editLastName').val(),
                            address: $('#editAddress').val(),
                            city: $('#editCity').val(),
                            email: $('#editEmail').val(),
                            phone: $('#editPhone').val(),
                            reelectionDate: $('#editDate').val(),
                            ttitle: $('#editTitle').val(),
                        }
                        console.log("DTY",editProfDto);
                    });
                });
            </script>
            <script type="text/javascript">
                $('#edit-professor').on('show.bs.modal',function (event) {
                    var button = $(event.relatedTarget)
                    var professor = $(event.relatedTarget).data('professor')
                    document.getElementById("editId").setAttribute("value", document.getElementById("profId").innerHTML);
                    document.getElementById("editFirstName").setAttribute("value", document.getElementById("profName").innerHTML);
                    document.getElementById("editLastName").setAttribute("value", document.getElementById("profLastName").innerHTML);
                    document.getElementById("editAddress").setAttribute("value", document.getElementById("profAddress").innerHTML);
                    document.getElementById("editCity").setAttribute("value", document.getElementById("profCity").innerHTML);
                    document.getElementById("editEmail").setAttribute("value", document.getElementById("profEmail").innerHTML);
                    document.getElementById("editPhone").setAttribute("value", document.getElementById("profPhone").innerHTML);
                    document.getElementById("editDate").setAttribute("value", document.getElementById("profDate").innerHTML);
                    document.getElementById("editTitle").setAttribute("value", document.getElementById("profTitle").innerHTML);
                })
            </script>
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Confirm deleting
                        </div>
                        <div class="modal-body">
                            Are you sure you want to delete this professor?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-danger btn-ok">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $('#confirm-delete').on('show.bs.modal', function (e) {
                    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                });
            </script>
        </div>
        <div id="menu2" class="container tab-pane fade"><br>
            <div class="my-tab-header">
                <h3 style="color: white">Students administration</h3>
                <h6 style="color: red">${message}</h6>
            </div>
            <div class="parent">
                <div class="divLeft">
                    <div class="form-wrapper">
                        <form:form action="${pageContext.request.contextPath}/student/insert" method="post" modelAttribute="studentDto">
                            <form:input path="idStudent" id="studentIdEdit" cssStyle="display: none" class="form-control"></form:input>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="">Index number</form:label>
                                    <form:input id="indexNumberEdit" path="indexNumber" type="text" class="form-control"
                                                placeholder="Index number"/>
                                    <form:errors path="indexNumber" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="firstName">First name</form:label>
                                    <form:input id="studentNameEdit" path="firstName" type="text" class="form-control"
                                                placeholder="First name"/>
                                    <form:errors path="firstName" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="lastName">Last name</form:label>
                                    <form:input path="lastName" id="studentLastNameEdit" type="text" class="form-control" placeholder="Last name"/>
                                    <form:errors path="lastName" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="email">Email</form:label>
                                    <form:input path="email" id="studentEmailEdit" type="text" class="form-control" placeholder="Email"/>
                                    <form:errors path="email" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="address">Address</form:label>
                                    <form:input path="address" id="studentAddressEdit" type="text" class="form-control" placeholder="Address"/>
                                    <form:errors path="address" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="city">City</form:label>
                                    <form:input path="city" id="studentCityEdit" type="text" class="form-control" placeholder="City"/>
                                    <form:errors path="city" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="phone">Phone</form:label>
                                    <form:input path="phone" id="studentPhoneEdit" type="text" class="form-control" placeholder="Phone"/>
                                    <form:errors path="phone" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="currentYearOfStudy">Study year</form:label>
                                    <form:input path="currentYearOfStudy" id="studentYearEdit" type="number" class="form-control"/>
                                    <form:errors path="currentYearOfStudy" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="align-items: center; justify-content: center">
                                <div style="text-align: center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
                <div class="divRight">
                    <div class="row">
                        <div style="text-align: center">
                            <form:form action="${pageContext.request.contextPath}/student/getAll" method="get">
                                <button type="submit" class="btn btn-primary">Load Students</button>
                            </form:form>
                        </div>
                    </div>
                    <div class="row">
                        <table class="table table-bordered">
                            <tr>
                                <th>#</th>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Index number</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            <c:forEach items="${studentDtoSet}" var="studentDto" varStatus="status">
                                <tr>
                                    <td id="studentId${status.index}">${studentDto.idStudent}</td>
                                    <td id="studentName${status.index}">${studentDto.firstName}</td>
                                    <td id="studentLastName${status.index}">${studentDto.lastName}</td>
                                    <td id="studentIndex${status.index}">${studentDto.indexNumber}</td>
                                    <td hidden id="studentEmail${status.index}">${studentDto.email}</td>
                                    <td hidden id="studentAddres${status.index}">${studentDto.address}</td>
                                    <td hidden id="studentCity${status.index}">${studentDto.city}</td>
                                    <td hidden id="studentPhone${status.index}">${studentDto.phone}</td>
                                    <td hidden id="studentYear${status.index}">${studentDto.currentYearOfStudy}</td>
                                    <td>
                                        <button style="font-size: 10px" class="btn btn-outline-primary" onclick="editStudent(${status.index})">
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                        <button style="font-size: 10px" class="btn btn-outline-danger"
                                                data-href="${pageContext.request.contextPath}/student/delete/${studentDto.idStudent}"
                                                data-toggle="modal" data-target="#confirm-deleteStudent">
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirm-deleteStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        Confirm deleting
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this student?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#confirm-deleteStudent').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        </script>
        <div id="menu3" class="container tab-pane fade"><br>
            <div class="my-tab-header">
                <h3 style="color: white">Subject Administration</h3>
                <h6 style="color: red">${message}</h6>
            </div>
            <div class="parent">
                <div class="divLeft">
                    <div class="form-wrapper">
                        <form:form action="${pageContext.request.contextPath}/subject/insert" method="post" modelAttribute="subjectDto">
                            <form:input id="idSubjectEdit" path="idSubject" cssStyle="display: none"></form:input>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col">
                                    <form:label path="subjectName">Subject name</form:label>
                                    <form:input id="subjectNameEdit" path="subjectName" type="text" class="form-control"
                                                placeholder="Subject name"/>
                                    <form:errors path="subjectName" cssClass="text-danger"/>
                                </div>
                                <div class="col">
                                    <form:label path="semester">Semester</form:label>
                                    <form:select id="semesterEdit" path="semester" class="form-control">
                                        <form:option value="NONE" label="Select semester" />
                                        <form:option value="Summer"/>
                                        <form:option value="Winter"/>
                                    </form:select>
                                    <form:errors path="semester" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                                <div class="col" style="max-width: 50%">
                                    <form:label path="yearOfStudy">Year of study</form:label>
                                    <form:input id="yearOfStudyEdit" path="yearOfStudy" type="number" class="form-control"/>
                                    <form:errors path="yearOfStudy" cssClass="text-danger"/>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1rem">
                            <div class="col">
                                <form:label path="description">Description</form:label>
                                <form:input id="descriptionEdit" path="description" class="form-control"
                                               placeholder="Description"/>
                                <form:errors path="description" cssClass="text-danger"/>
                            </div>
                            </div>
                            <div class="row" style="align-items: center; justify-content: center">
                                <div style="text-align: center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
                <div class="divRight">
                    <div class="row">
                        <div style="text-align: center">
                            <form:form action="${pageContext.request.contextPath}/subject/getAll" method="get">
                                <button type="submit" class="btn btn-primary">Load Subjects</button>
                            </form:form>
                        </div>
                    </div>
                    <div class="row">
                        <table id="tableSubject" class="table table-bordered">
                            <tr>
                                <th>#</th>
                                <th>Subject name</th>
                                <th>Year</th>
                                <th>Semester</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            <c:forEach items="${subjectDtoSet}" var="subjectDto" varStatus="status">
                                <tr>
                                    <td value="${subjectDto.idSubject}" id="subjectId${status.index}">${subjectDto.idSubject}</td>
                                    <td id="subjectName${status.index}">${subjectDto.subjectName}</td>
                                    <td id="subjectYear${status.index}">${subjectDto.yearOfStudy}</td>
                                    <td id="semester${status.index}">${subjectDto.semester}</td>
                                    <td style="display: none" id="subjectDesc${status.index}">${subjectDto.description}</td>
                                    <td>
                                        <button style="font-size: 10px" class="btn btn-outline-primary" onclick="editSubject(${status.index})">
                                           Edit
                                        </button>
                                    </td>
                                    <td>
                                        <button style="font-size: 10px" class="btn btn-outline-danger"
                                                data-href="${pageContext.request.contextPath}/subject/delete/${subjectDto.idSubject}"
                                                data-toggle="modal" data-target="#confirm-deleteSubject">
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirm-deleteSubject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        Confirm deleting
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this subject?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#confirm-deleteSubject').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        </script>
        <div id="menu4" class="container tab-pane fade"><br>
            <div class="my-tab-header">
                <h3 style="color: white">Exam Administration</h3>
                <h6 style="color: red">${message}</h6>
            </div>
            <div class="parentExam">
                <div class="divTop">
                   <div class="parent">
                       <div class="divLeft" style="display: flex">
                           <div class="form-wrapper">
                            <form:form action="${pageContext.request.contextPath}/exam/insert" method="post" modelAttribute="examDto">
                                <div class="row" style="margin-bottom: 1rem">
                                    <label>Professor</label>
                                    <form:select name="professorDrop" id="professorDrop" path="idProfessor" class="form-control">
                                        <c:forEach items="${professorForExam}" var="professors">
                                            <form:option value="${professors.id}"><c:out value="${professors.firstName} ${professors.lastName}"/></form:option>
                                        </c:forEach>
                                    </form:select>
                                    <form:errors path="idProfessor" cssClass="text-danger"/>
                                </div>
                                <div class="row" style="margin-bottom: 1rem">
                                    <label>Subject</label>
                                    <form:select id="subjectDrop" name="subjectDrop" path="idSubject" class="form-control">
                                        <c:forEach items="${subjectsForExam}" var="subject">
                                            <form:option value="${subject.idSubject}"><c:out value="${subject.subjectName} ${subject.yearOfStudy}"/></form:option>
                                        </c:forEach>
                                    </form:select>
                                    <form:errors path="idSubject" cssClass="text-danger"/>
                                </div>
                                <div class="row" style="margin-bottom: 1rem">
                                    <div>
                                        <form:label path="dateOfExam">Date of exam</form:label>
                                        <form:input path="dateOfExam" type="date" class="form-control datepicker"/>
                                        <form:errors path="dateOfExam" cssClass="text-danger"/>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 1rem">
                                    <button style="width: 40%; font-size: 10px"  type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form:form>
                            </div>
                           <div style="width: 30%; margin-left: 5%; display: grid; height: 53%;margin-top:2%">
                               <button style="width: 100%;margin: auto" class="btn btn-primary" onclick="searchByProfessorId()">Search by prof</button>
                               <button style="width: 100%; margin: auto" class="btn btn-primary" onclick="searchBySubjectId()">Search by subject</button>
                           </div>
                        </div>
                       <div class="divRight">
                           <div class="parentExam">
                               <div style="display: flex">
                                   <p style="margin-right: 1rem">You want to register:</p>
                                   Subject:<p style="margin-left: 1rem" id="paragrafSubject"></p>
                                   Profesor:<p style="margin-left: 1rem" id="paragrafProfessor"></p>
                               </div>
                               <div style="display:flex;flex-direction: row-reverse">
                                   <form:form modelAttribute="examRegistrationRequestDto" action="${pageContext.request.contextPath}/examRegistration/insert" method="post" cssStyle="width: 50%;margin: auto">
                                       <div class="row">
                                           <form:label path="studentIndex">Student index</form:label>
                                           <form:input path="studentIndex" cssClass="form-control"/>
                                           <form:errors path="studentIndex" cssClass="text-danger"/>
                                       </div>
                                       <div class="row">
                                           <label>Today date</label>
                                           <form:input type="text" path="dateOfRegistration" cssClass="form-control" id="dateForRegistration"/>
                                           <form:errors path="dateOfRegistration" cssClass="text-danger"/>
                                       </div>
                                       <div>
                                           <form:input type="hidden" path="examId" cssClass="form-control" id="idExamRegistration"/>
                                       </div>
                                       <div class="row">
                                           <button style="margin: auto" type="submit" class="btn btn-primary">Register</button>
                                       </div>
                                   </form:form>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
                <div class="divBot">
                    <div class="my-tab-header">
                        <h3 style="color: white">Registration of the exam on behalf of the student</h3>
                        <h6 style="color: red">${message}</h6>
                    </div>
                    <div class="row" style="margin-bottom: 1rem">
                        <div class="col-sm-1">
                            <label style="display: block ruby">Page size</label>
                            <select id="pageSize" name="pageSize" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                <option value="5" selected="selected">5</option>
                                <option value="20">10</option>
                                <option value="30">20</option>
                            </select>
                        </div>
                        <button style="margin-top: 2rem" class="btn btn-primary" onclick="getAllExams(0)">Get all exams</button>
                    </div>
                    <div>
                        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="th-sm">id

                                </th>
                                <th class="th-sm">Professor name

                                </th>
                                <th class="th-sm">Subject name

                                </th>
                                <th class="th-sm">Date of exam

                                </th>
                                <th class="th-sm">Expired

                                </th>
                                <th class="th-sm">Make registration

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${searchedExams}" var="exam" varStatus="status">
                                    <tr>
                                        <td id="examId${status.index}">${exam.idExamDtoResponse}</td>
                                        <td id="professorExam${status.index}">${exam.professorDto.firstName}</td>
                                        <td id="subjectExam${status.index}">${exam.subjectDto.subjectName}</td>
                                        <td id="examDate${status.index}">${exam.dateOfExam}</td>
                                        <td id="examExpired${status.index}">${exam.expired}</td>
                                        <td>
                                            <button onclick="setValuesRegistration(${exam.idExamDtoResponse},'${exam.subjectDto.subjectName}','${exam.professorDto.firstName}')" style="font-size: 10px" class="btn btn-outline-primary">
                                                Registration
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>id
                                </th>
                                <th>Professor name
                                </th>
                                <th>Subject name
                                </th>
                                <th>Date of exam
                                </th>
                                <th>Expired
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row" style="margin: auto; flex-direction: row-reverse;">
                        <ul class="pagination">
                            <li class="paginate_button page-item "><a onclick="getAllExams(1)" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                            <li class="paginate_button page-item "><a onclick="getAllExams(2)" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">2</a></li>
                            <li class="paginate_button page-item "><a onclick="getAllExams(3)" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">3</a></li>
                            <li class="paginate_button page-item "><a onclick="getAllExams(4)" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">4</a></li>
                            <li class="paginate_button page-item "><a onclick="getAllExams(5)" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">5</a></li>
                            <li class="paginate_button page-item "><a onclick="getAllExams(6)" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">6</a></li>
                        </ul>
                    </div>
                    <script>

                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
