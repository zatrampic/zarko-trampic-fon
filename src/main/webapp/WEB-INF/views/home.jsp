<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css">
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
          media="screen"
          th:href="@{${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css}"/>
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"
            th:src="@{${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js}"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div style="display:flex;justify-content: center; color: #007bff; font-family: 'Agency FB'"><h1>Security demo</h1></div>
    <div class="login-center">
        <h3>Here u can try to go on admin page without login!</h3>
        <h6>if you are not logged u will be redirected to login page.</h6>
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/admin">Admin</a>
        <h3>Or u can access to login page where you can sing in.</h3>
        <a href="${pageContext.request.contextPath}/login">Login</a>
        <a href="${pageContext.request.contextPath}/singUp">Sing up</a>
    </div>
</body>
</html>