<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css">
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
          media="screen"
          th:href="@{${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css}"/>
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"
            th:src="@{${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js}"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body class="login-center">
<h1 style="color: red">${message}</h1>
<%--@elvariable id="credentials" type="zarko.trampic.fon.dto.UserCredentials"--%>
<form:form action="login" method="POST" modelAttribute="credentials">
    <form:label path="username">Username</form:label>
    <form:input path="username" cssClass="form-control"/>
    <form:label path="password">Password</form:label>
    <form:input path="password" cssClass="form-control"/>
    <input class="btn btn-primary" type="submit" value="Login" style="margin-top: 1rem">
</form:form>
<a href="${pageContext.request.contextPath}/singUp">Sing up</a>
</body>
</html>
