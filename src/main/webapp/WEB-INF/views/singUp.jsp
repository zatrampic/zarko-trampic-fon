<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sing up</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css">
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
          media="screen"
          th:href="@{${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css}"/>
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"
            th:src="@{${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js}"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body class="login-center">
<h1>Sing up!</h1>
<form:form action="${pageContext.request.contextPath}/singUp" method="POST" modelAttribute="userDto" cssStyle="display: grid">
    <form:label path="username">Username</form:label>
    <form:input path="username" cssClass="form-control" required="required"/>
    <form:errors path="username" cssClass="text-danger"/>
    <br>
    <br>
    <form:label path="password">Password</form:label>
    <form:input path="password" cssClass="form-control" required="required"/>
    <form:errors path="password" cssClass="text-danger"/>
    <br>
    <br>
    <form:label path="firstName">First name</form:label>
    <form:input path="firstName" cssClass="form-control" required="required"/>
    <form:errors path="firstName" cssClass="text-danger"/>
    <br>
    <br>
    <form:label path="lastName">Last name</form:label>
    <form:input path="lastName" cssClass="form-control" required="required"/>
    <form:errors path="lastName" cssClass="text-danger"/>
    <br>
    <br>
    <input class="btn btn-primary" type="submit" value="Sing up" style="margin-top: 1rem">
</form:form>

<h1>${message}</h1>
</body>
</html>
