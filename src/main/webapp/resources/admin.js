
function editSubject(status) {
    console.log("STATUS",status)
    document.getElementById("idSubjectEdit").setAttribute("value", document.getElementById("subjectId"+status).innerHTML);
    document.getElementById("subjectNameEdit").setAttribute("value", document.getElementById("subjectName"+status).innerHTML);
    document.getElementById("yearOfStudyEdit").setAttribute("value", document.getElementById("subjectYear"+status).innerHTML);
    document.getElementById("descriptionEdit").setAttribute("value", document.getElementById("subjectDesc"+status).innerHTML);
    var semId = document.getElementById("semester"+status).innerHTML;
    console.log("semId",semId);
    console.log("semId",document.getElementById("semesterEdit"));
    if(semId === "Winter"){
        document.getElementById("semesterEdit").selectedIndex = 2;
    }else{
        document.getElementById("semesterEdit").selectedIndex = 1;
    }
}

function editStudent(status) {
    document.getElementById("studentIdEdit").setAttribute("value", document.getElementById("studentId"+status).innerHTML);
    document.getElementById("studentNameEdit").setAttribute("value", document.getElementById("studentName"+status).innerHTML);
    document.getElementById("studentLastNameEdit").setAttribute("value", document.getElementById("studentLastName"+status).innerHTML);
    document.getElementById("indexNumberEdit").setAttribute("value", document.getElementById("studentIndex"+status).innerHTML);
    document.getElementById("studentEmailEdit").setAttribute("value", document.getElementById("studentEmail"+status).innerHTML);
    document.getElementById("studentAddressEdit").setAttribute("value", document.getElementById("studentAddres"+status).innerHTML);
    document.getElementById("studentCityEdit").setAttribute("value", document.getElementById("studentCity"+status).innerHTML);
    document.getElementById("studentPhoneEdit").setAttribute("value", document.getElementById("studentPhone"+status).innerHTML);
    document.getElementById("studentYearEdit").setAttribute("value", document.getElementById("studentYear"+status).innerHTML);
}
