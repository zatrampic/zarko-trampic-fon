
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Table structure for table `approle`
--

CREATE TABLE `approle` (
  `id` bigint(20) NOT NULL,
  `roleName` enum('ROLE_ADMIN','ROLE_USER') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `idExam` bigint(20) NOT NULL,
  `examDate` date DEFAULT NULL,
  `expired` bit(1) NOT NULL,
  `professor_idProfessor` bigint(20) DEFAULT NULL,
  `subject_idSubject` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `examregistration`
--

CREATE TABLE `examregistration` (
  `idExamRegistration` bigint(20) NOT NULL,
  `dateOfRegistration` datetime DEFAULT NULL,
  `studentIndex` varchar(255) DEFAULT NULL,
  `exam_idExam` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `id` bigint(20) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `professor`
--

CREATE TABLE `professor` (
  `idProfessor` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `reelectionDate` date NOT NULL,
  `city` bigint(20) DEFAULT NULL,
  `title` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `idStudent` bigint(20) NOT NULL,
  `address` varchar(50) DEFAULT NULL,
  `currentYearOfStudy` bigint(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `firstName` varchar(30) NOT NULL,
  `indexNumber` varchar(10) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `phoneNumber` varchar(15) DEFAULT NULL,
  `city` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `idSubject` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `subjectName` varchar(255) NOT NULL,
  `yearOfStudy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE `title` (
  `idTitle` bigint(20) NOT NULL,
  `titleName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `userName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_approle`
--

CREATE TABLE `user_approle` (
  `User_id` bigint(20) NOT NULL,
  `appRoles_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `approle`
--
ALTER TABLE `approle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `databasechangeloglock`
--
ALTER TABLE `databasechangeloglock`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`idExam`),
  ADD KEY `FKmu59yfjufme4gjfdactcpc4dt` (`professor_idProfessor`),
  ADD KEY `FKfcl125lto5loxuwcxor9tuj0h` (`subject_idSubject`);

--
-- Indexes for table `examregistration`
--
ALTER TABLE `examregistration`
  ADD PRIMARY KEY (`idExamRegistration`),
  ADD KEY `FKc36vg8bj2o7eqwin2pxqi06ui` (`exam_idExam`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_pxjeu51ml16br41gcc68rfliq` (`city`);

--
-- Indexes for table `professor`
--
ALTER TABLE `professor`
  ADD PRIMARY KEY (`idProfessor`),
  ADD UNIQUE KEY `UK_47iiro4c3fqtgbdw7u9655wsn` (`email`),
  ADD KEY `FKnvju928wag1oi1stpobsbokko` (`city`),
  ADD KEY `FKpn2amxndwv8axfa2lno2wmr4k` (`title`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`idStudent`),
  ADD UNIQUE KEY `UK_2nplagbokino1ntpvvsn5kcdy` (`indexNumber`),
  ADD UNIQUE KEY `UK_msrnvlmsye9t98fb3bvekffiq` (`email`),
  ADD KEY `FKorycu37ts4fot3yu57dqr8equ` (`city`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`idSubject`);

--
-- Indexes for table `title`
--
ALTER TABLE `title`
  ADD PRIMARY KEY (`idTitle`),
  ADD UNIQUE KEY `UK_oibltjyem3kj1uh2e18xns2jt` (`titleName`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_hl8fftx66p59oqgkkcfit3eay` (`userName`);

--
-- Indexes for table `user_approle`
--
ALTER TABLE `user_approle`
  ADD PRIMARY KEY (`User_id`,`appRoles_id`),
  ADD KEY `FKrlfsc98ay5hnst5xcmkom97gh` (`appRoles_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `approle`
--
ALTER TABLE `approle`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `idExam` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `examregistration`
--
ALTER TABLE `examregistration`
  MODIFY `idExamRegistration` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `professor`
--
ALTER TABLE `professor`
  MODIFY `idProfessor` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `idStudent` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `idSubject` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `title`
--
ALTER TABLE `title`
  MODIFY `idTitle` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `exam`
--
ALTER TABLE `exam`
  ADD CONSTRAINT `FKfcl125lto5loxuwcxor9tuj0h` FOREIGN KEY (`subject_idSubject`) REFERENCES `subject` (`idSubject`),
  ADD CONSTRAINT `FKmu59yfjufme4gjfdactcpc4dt` FOREIGN KEY (`professor_idProfessor`) REFERENCES `professor` (`idProfessor`);

--
-- Constraints for table `examregistration`
--
ALTER TABLE `examregistration`
  ADD CONSTRAINT `FKc36vg8bj2o7eqwin2pxqi06ui` FOREIGN KEY (`exam_idExam`) REFERENCES `exam` (`idExam`);

--
-- Constraints for table `professor`
--
ALTER TABLE `professor`
  ADD CONSTRAINT `FKnvju928wag1oi1stpobsbokko` FOREIGN KEY (`city`) REFERENCES `place` (`id`),
  ADD CONSTRAINT `FKpn2amxndwv8axfa2lno2wmr4k` FOREIGN KEY (`title`) REFERENCES `title` (`idTitle`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `FKorycu37ts4fot3yu57dqr8equ` FOREIGN KEY (`city`) REFERENCES `place` (`id`);

--
-- Constraints for table `user_approle`
--
ALTER TABLE `user_approle`
  ADD CONSTRAINT `FK32fqou7uin9c0ugvevh9aijny` FOREIGN KEY (`User_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKrlfsc98ay5hnst5xcmkom97gh` FOREIGN KEY (`appRoles_id`) REFERENCES `approle` (`id`);
COMMIT;
